'use strict';

const data = async (data,description=null,code=200) => {
    return {err:null,data:data,code:code,message:description};
}

const paginationData = async (data,meta,description=null,code=200) => {
    return {err:null,data:data,meta:meta,code:code,message:description};
}

const error = async (err,description,code=500) => {
    return {err:err,data:null,code:code,message:description};
}

const response = async (res,type,result,message=null,code=null) => {
    if(message){
        result.message = message;
    }
    if(code){
        result.code = code;
    } 
    let status = `error`;
    switch(type){
    case 'fail':
        status = 'fail';
        break;
    case 'success':
        status = 'success';
        break;
    }
    res.send(
        {
        status: status,
        data: result.data,
        code: result.code,
        message: result.message
        }
    );
}

const paginationResponse = async (res,type,result,message=null,code=null) => {
    if(message){
         result.message = message;
    }
    if(code){
        result.code = code;
    } 
    let status = `error`;
    switch(type){
    case 'fail':
        status = 'fail';
        break;
    case 'success':
        status = 'success';
        break;
    }
    res.send(
        {
        status: status,
        data: result.data,
        meta: result.meta,
        code: result.code,
        message: result.message
        }
    );
}

module.exports = {
    data: data,
    paginationData: paginationData,
    error: error,
    response: response,
    paginationResponse: paginationResponse
}
