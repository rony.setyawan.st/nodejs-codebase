'use strict';

const wrapper = require('../../../../helpers/utils/wrapper');
const validator = require('../../utils/brand_validator');
const Brand = require('./domain');

const tlu_InveCPBrandCreatedCommand = async (message) => {
    const validatePayload = await validator.tlu_InveCPBrandPayLoadIsValid(message);
    const validateIfExist = async (result) => {
        if(result.err){
            return result;
        }else{
            return await validator.tlu_InveCPBrandIfExist(message);
        }
    }
    const postData = async (result) => {
        if((result.err) && (result.code!==404)) {
            return result;
        }else if (result.code!==404){
            return result;
        }else{
            const brand = new Brand();
            const result = await brand.addNewBrandFromTlu_InveCPBrand(message);
            if(result.err){
                return result;
            }else{
                brand.publishEventBrandCreatedFromTlu_InveCPBrand(message);
                return result;
            }
        }
    }
    const response = await postData(await validateIfExist(validatePayload));
    return response;
}

const tlu_InveCPBrandEventingCreatedCommand = async (message) => {
    const validatePayload = await validator.tlu_InveCPBrandPayLoadIsValid(message);
    const validateIfExist = async (result) => {
        if(result.err) {
            return result;
        } else {
            return await validator.tlu_InveCPBrandEventStoreIfExist(message);
        }
    }
    const postData = async (result) => {
        if((result.err) && (result.code!==404)){
            return result;
        }else if(result.code!==404){
            return result;
        }else{
            const brand = new Brand();
            const result = await brand.addNewBrandFromTlu_InveCPBrandToEventStore(message);
            if(result.err){
                return result;
            }else{
                brand.publishEventBrandCreatedFromTlu_InveCPBrandToEventStore(result.data);
                return result;
            }
        }
    }
    const response = await postData(await validateIfExist(validatePayload));
    return response;
}

const tlu_InveCPBrandUpdatedCommand = async (message) => {
    const validatePayload = await validator.tlu_InveCPBrandPayLoadIsValid(message)
    const validateIfExist = async (result) => {
        if (result.err) {
            return result
        } else {
            return await validator.tlu_InveCPBrandIfExist(message)
        }
    }
    const postData = async (result) => {
        if((result.err) && (result.code!==404)) {
            return result
        }else{
            const brand = new Brand()
            const result = await brand.editNewBrandFromTlu_InveCPBrand(message)
            if(result.err){
                return result
            }else{
                brand.publishEventBrandUpdatedFromTlu_InveCPBrand(message)
                return result
            }
        }
    }
    const response = await postData(await validateIfExist(validatePayload))
    return response
}

const tlu_InveCPBrandEventingUpdatedCommand = async (message) => {
    const validatePayload = await validator.tlu_InveCPBrandPayLoadIsValid(message)
    const validateIfExist = async (result) => {
        if(result.err){
            return result
        } else {
            return await validator.tlu_InveCPBrandEventStoreIfExist(message)
        }
    }
    const postData = async (result) => {
        if(result.err && result.code !== 404){
            return result
        }else{
            const brand = new Brand()
            const result = await brand.editNewBrandFromTlu_InveCPBrandToEventStore(message)
            if(result.err){
                return result
            }else{
                brand.publishEventBrandUpdatedFromTlu_InveCPBrandToEventStore(result.data)
                return result
            }
        }
    }
    const response = await postData(await validateIfExist(validatePayload))
    return response
}

module.exports = {
    tlu_InveCPBrandCreatedCommand: tlu_InveCPBrandCreatedCommand,
    tlu_InveCPBrandEventingCreatedCommand: tlu_InveCPBrandEventingCreatedCommand,
    tlu_InveCPBrandUpdatedCommand: tlu_InveCPBrandUpdatedCommand,
    tlu_InveCPBrandEventingUpdatedCommand: tlu_InveCPBrandEventingUpdatedCommand
}