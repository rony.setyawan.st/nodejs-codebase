'use strict';

const Brand = require('./domain');
const wrapper = require('../../../../helpers/utils/wrapper');

const queryOneBrand = async (id) => {
    const brand = new Brand(id);
    const result = await brand.viewOneBrand();
    return result;
}

const queryManyBrand = async (queryParser) => {
    const brand = new Brand();
    const result = await brand.viewManyBrand(queryParser);
    return result;
}

const queryBrandName = async (name) => {
    const brand = new Brand();
    const result = await brand.viewOneBrandName(name)
    return result
}

module.exports = {
    queryOneBrand: queryOneBrand,
    queryManyBrand: queryManyBrand,
    queryBrandName: queryBrandName
}