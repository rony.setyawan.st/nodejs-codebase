'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');

const queryParserGetManyBrand = async (req) => {
    let brand = req.params.brandId
    let param;
    if(validate.isEmpty(brand)){
        return param
    }else if(!validate.isArray(brand)){
        brand = brand.toUpperCase()
        param = {'id':brand}
        return param
    }else if(validate.isArray(brand)){
        brand = String.prototype.toUpperCase.apply(brand).split(',')
        param = {'id':{$in:brand}}
        return param
    }else{
        return param
    }
}

const parseGetManyBrand = async (req) => {
    const brandId = req.params.brand;
    if(validate.isEmpty(brandId)){
        return ``;
    }else if(!validate.isArray(brandId)){
        return `WHERE vBrandID='${brandId}'`;
    }else if(validate.isArray(brandId)){
        let data = ``;
        brandId.map(oneBrandId => {
            (validate.isEmpty(data)) ? data = `'${oneBrandId}'` : data = `${data},'${oneBrandId}'`;
        });
        return `WHERE vBrandID IN (${data})`;
    }else{
        return ``;
    }    
}

const parseGetManySKU = async (req) => {
    const sku = req.params.sku;
    if(validate.isEmpty(sku)){
        return ``;
    }else if(!validate.isArray(sku)){
        return `WHERE vPartID='${sku}'`;
    }else if(validate.isArray(sku)){
        let data = ``;
        sku.map(oneSku => {
            (validate.isEmpty(data)) ? data = `'${oneSku}'` : data = `${data},'${oneSku}'`;
        });
        return `WHERE vPartID IN (${data})`;
    }else{
        return ``;
    }    
}

module.exports = {
  queryParserGetManyBrand: queryParserGetManyBrand,
  parseGetManyBrand: parseGetManyBrand,
  parseGetManySKU: parseGetManySKU
}