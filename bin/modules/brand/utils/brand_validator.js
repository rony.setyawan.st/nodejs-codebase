'use strict';

const validate = require("validate.js");
const wrapper = require('../../../helpers/utils/wrapper');
const MSSQL = require('../../../helpers/databases/mssql/db');
const Mongo = require('../../../helpers/databases/mongodb/db');
const config = require('../../../infra/configs/global_config');

const validateConstraints = async (values,constraints) => {
    if(validate(values,constraints)){
        return wrapper.error(`Bad Request`,validate(values,constraints),400);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamGetOneBrand = async (req) => {
    const brandId = req.params.id;
    let constraints = {};
    let values = {};
    if(validate.isEmpty(brandId)){
        return wrapper.error(`Bad Request`,`Parameter Brand Id is Required`,400);
    }else{
        constraints[brandId] = {length: {is: 3}};
        values[brandId] = brandId;
        return validateConstraints(values,constraints);
    }
}

const isValidParamGetManyBrand = async (req) => {
    const brandId = req.params.id;
    let constraints = {};
    let values = {};
    if(validate.isEmpty(brandId)){
        return wrapper.data(true);
    }else if(!validate.isArray(brandId)){
        constraints[brandId] = {length: {is: 3}};
        values[brandId] = brandId;
        return validateConstraints(values,constraints);
    }else if(validate.isArray(brandId)){
        brandId.map(oneBrandId => {
            constraints[oneBrandId] = {length: {is: 3}};
            values[oneBrandId] = oneBrandId;
        });
        return validateConstraints(values,constraints);
    }else{
        return wrapper.data(true);
    }
}

const isValidParamPostOneBrand = async (req) => {
    const brandPayload = req.body;
    if(!validate.isObject(brandPayload)){
        return wrapper.error(`Bad Request`,`Parameter Brand Payload is Required and must be JSON Object`,400);
    }else if(validate.isEmpty(brandPayload.id)){
        return wrapper.error(`Bad Request`,`Parameter Brand Id is Required`,400);
    }else if(validate.isEmpty(brandPayload.code)){
        return wrapper.error(`Bad Request`,`Parameter Brand Code is Required`,400);
    }else if(validate.isEmpty(brandPayload.name)){
        return wrapper.error(`Bad Request`,`Parameter Brand Name is Required`,400);
    }else{
        return wrapper.data(true);
    }
}

const tlu_InveCPBrandPayLoadIsValid = async (message) => {
    const vBrandID = message.payload.vBrandID;
    let constraints = {};
    let values = {};
    if(validate.isEmpty(vBrandID)){
        return wrapper.error(`Bad Request`,`Parameter vBrandID is Required ${JSON.stringify(message)}`,400);
    }else{
        constraints[vBrandID] = {length: {is: 3}};
        values[vBrandID] = vBrandID;
        return validateConstraints(values,constraints);
    }
}

const tlu_InveCPBrandIfExist = async (message) => {
    const vBrandID = message.payload.vBrandID;
    const db = new Mongo(config.getMongoCoreDOS());
    db.setCollection('tlu_InveCPBrand');
    const parameter = {"payload.vBrandID":vBrandID};
    const result = await db.findOne(parameter);
    return result;
}

const tlu_InveCPBrandEventStoreIfExist = async (message) => {
    const id = message.payload.id;
    const db = new Mongo(config.getMongoProductES());
    db.setCollection('brand');
    const parameter = {"payload.id":id};
    const result = await db.findOne(parameter);
    return result;
}

module.exports = {
 isValidParamGetOneBrand: isValidParamGetOneBrand,
 isValidParamGetManyBrand: isValidParamGetManyBrand,
 isValidParamPostOneBrand: isValidParamPostOneBrand,
 tlu_InveCPBrandPayLoadIsValid: tlu_InveCPBrandPayLoadIsValid,
 tlu_InveCPBrandIfExist: tlu_InveCPBrandIfExist,
 tlu_InveCPBrandEventStoreIfExist: tlu_InveCPBrandEventStoreIfExist

}